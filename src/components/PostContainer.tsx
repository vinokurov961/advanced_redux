import React, { useEffect, useState } from "react";
import { IPost } from "../models/iPost";
import { postAPI } from "./../services/PostSevice";
import PostItem from "./PostItem";

const PostContainer = () => {
  const [limit, setLimit] = useState(100);

  const {
    data: posts,
    error,
    isLoading,
    refetch//=> функция для переотправки запроса
  } = postAPI.useFetchAllPostsQuery(limit, {
    // pollingInterval: 1000//=> для потворения запроса каждую секунду
  });

  const [createPost, {}] = postAPI.useCreatePostMutation()
  const [deletePost, {}] = postAPI.useDeletePostMutation()
  const [updatePost, {}] = postAPI.useUpdatePostMutation()

  useEffect(() => {
    // setTimeout(()=>{
    //   setLimit(3)
    // }, 2000)
  },[])

  const handleCreate = async () => {
    const title = prompt()
    await createPost({ title, body: title } as IPost);
  }

  const handleRemove = (post: IPost) => {
    deletePost(post)
  }
  const handleUpdate = (post: IPost) => {
    updatePost(post)
  }

  return (
    <div>
      <button onClick={handleCreate}>Add new Post</button>
      <button onClick={() => refetch()}>REFETCH</button>
      {isLoading && <h1>Идет загрузка...</h1>}
      {error && <h1>Произошла ошибка...</h1>}
      {posts?.map((post) => (
        <PostItem
          remove={handleRemove}
          update={handleUpdate}
          key={post.id}
          post={post}
        />
      ))}
    </div>
  );
};

export default PostContainer;
