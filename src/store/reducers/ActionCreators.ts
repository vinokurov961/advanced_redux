import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { IUser } from "../../models/IUser";
// import { AppDispatch } from "../store";
// import { UserSlice } from "./UserSlice";

// export const fetchUsers = () => async (dispatch: AppDispatch) => {
//   try {
//     dispatch(UserSlice.actions.userFetching());
//     const response = await axios.get<IUser[]>(
//       "https://jsonplaceholder.typicode.com/users"
//     );
//     dispatch(UserSlice.actions.userFetchingSuccess(response.data));
//   } catch (e: unknown) {
//     const u = e as { massage: string };
//     dispatch(UserSlice.actions.userFetchingError(u.massage));
//   }
// };

export const fetchUsers = createAsyncThunk(
  "user/fetchAll",
  async (_, thunkApi) => {
    try {
      const response = await axios.get<IUser[]>(
        "https://jsonplaceholder.typicode.com/users"
      );
      return response.data;
    } catch(e) {
      return thunkApi.rejectWithValue("Не удалось загрузить посты")
    }
  }
);
