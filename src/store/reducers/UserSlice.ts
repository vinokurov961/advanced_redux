import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { IUser } from "../../models/IUser";
import { fetchUsers } from "./ActionCreators";

interface UserState {
  users: IUser[];
  isLoading: boolean;
  error: string;
}

const initialState: UserState = {
  users: [],
  isLoading: false,
  error: "",
};

export const UserSlice = createSlice({
  name: "user",
  initialState,
  reducers: {},
  extraReducers: {
    [fetchUsers.fulfilled.type]: (
      state: UserState,
      action: PayloadAction<IUser[]>
    ) => {
      //
      state.isLoading = false;
      state.error = "";
      state.users = action.payload;
    },
    [fetchUsers.pending.type]: (state: UserState) => {
      //когда идет ожидание
      state.isLoading = true;
    },
    [fetchUsers.rejected.type]: (
      state: UserState,
      action: PayloadAction<string>
    ) => {
      //когда произошла ошибка
      state.isLoading = false;
      state.error = action.payload;
    },
  },
});

export default UserSlice.reducer;
